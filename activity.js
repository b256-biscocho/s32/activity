const http = require("http");

const port = 4000;

const server = http.createServer(function(req, res) {
  if (req.url == "/" && req.method == "GET") {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Welcome to Booking System');
    res.end();
  }

  if (req.url == "/profile" && req.method == "GET") {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Welcome to your profile!');
    res.end();
  }

  if (req.url == "/courses" && req.method == "GET") {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Here’s our courses available');
    res.end();
  }

  if (req.url == "/addcourse" && req.method == "POST") {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Add a course to our resources');
    res.end();
  }

  if (req.url == "/updatecourse" && req.method == "PUT") {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Update a course to our resources');
    res.end();
  }

  if (req.url == "/archivecourses" && req.method == "DELETE") {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Archive courses to our resource');
    res.end();
  }
});

server.listen(port);
console.log(`Server is running at localhost:${port}`);